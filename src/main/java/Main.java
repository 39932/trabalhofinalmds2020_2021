package main.java;
public class Main {

    public static void  main(String [] args)
    {
        DB database= new DB();
        Organizador zeca = new Organizador("Zeca","zequinha@ola.pt");
        Revisor amigo1 = new Revisor("Amigo 1","bruh","hue");
        Revisor amigo2 = new Revisor("Amigo 2","42","asds");
        Revisor amigo3 = new Revisor("Amigo 2","42","asds");
        Participante p1= new Participante("asdad","sadsa");
        Participante p2= new Participante("1999asdad","sadsa");
        p1.addParticipanteToBD(database,p1);
        p2.addParticipanteToBD(database,p2);
        amigo1.addRevisorToBD(database,amigo1);
        amigo2.addRevisorToBD(database,amigo2);
        amigo3.addRevisorToBD(database,amigo3);
        Autor autor2= new Autor("bahhhhhhh","bahhhhhhhhj","bruhhhhh");
        Artigo artigo1 = new Artigo("um dia feliz de amizade","Amor é belo",autor2);
        Artigo artigo2 = new Artigo("um dia nas palmeiras","They bless the rain down in africa", autor2);
        Artigo artigo3 = new Artigo("um dia nas palmeiras2","They bless the rain down in africa", autor2);
        autor2.addArtigoToBD(database,artigo1);
        autor2.addArtigoToBD(database,artigo2);
        autor2.addArtigoToBD(database,artigo3);
        zeca.setNotaMinimaAprovacao();
        zeca.avaliacaoArtigos(database);
        zeca.printArtigos(database);
        zeca.printInscricoes(database);
    }
}
