package main.java;

public class Participante {

    String nome;
    String email;

    public Participante(String nome, String email)
    {
        this.nome=nome;
        this.email=email;
    }

    public void setName(String nome){
        this.nome=nome;
    }

    public void setEmail(String email)
    {
        this.email=email;
    }

    public String getName()
    {
        return nome;
    }

    public String getEmail(){
        return email;
    }

    public void addParticipanteToBD(DB database,Participante participante){
        database.addInscricaoToDB(participante);
    }
}
