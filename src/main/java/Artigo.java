package main.java;
public class Artigo {
    String artigo;
    Autor autor;
    String titulo;
    short numeroDeRevisoes;
    int notaAtual;
    boolean aprovacao;

    public Artigo(String artigo,String titulo,Autor autor)
    {
        this.artigo=artigo;
        this.titulo=titulo;
        this.autor=autor;
        numeroDeRevisoes=0;
        int notaAtual=0;
        aprovacao=false;
    }

    public void setArtigo( String artigo){
        this.artigo=artigo;
    }

    public void setTitulo(String titulo){
        this.titulo=titulo;
    }

    public void setAutores(Autor autor){
        this.autor=autor;
    }

    public void setNumeroDeRevisoes(short n1){
        this.numeroDeRevisoes=n1;
    }

    public short getNumeroDeRevisoes(){
        return this.numeroDeRevisoes;
    }

    public String getArtigo(){
        return this.artigo;
    }

    public String getTitulo (){
        return this.titulo;
    }

    public Autor getAutores() {
        return this.autor;
    }

}
