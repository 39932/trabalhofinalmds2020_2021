package main.java;
import java.util.*;


public class DB {

    //database de artigos
    ArrayList<Artigo> artigoDB;
    //database de revisores
    ArrayList<Revisor> revisorDB;
    //database de inscrições na conferência
    ArrayList<Participante> inscricoesDB;
    int artigoAtual;

    public DB(){
        this.artigoDB= new ArrayList<>();
        this.revisorDB= new ArrayList<>();
        this.inscricoesDB= new ArrayList<>();
        this.artigoAtual=0;
    }

    public void addInscricaoToDB(Participante participante){
        this.inscricoesDB.add(participante);
    }

    public void addArtigoToDB(Artigo artigo){
        this.artigoDB.add(artigo);
    }

    public void addRevisorToDB(Revisor revisor){
        this.revisorDB.add(revisor);
    }


    public void setArtigo(Artigo artigo,int contador){
        artigoDB.set(contador,artigo);
    }

    public Artigo getArtigoDB(Artigo artigo1){

        for (int i=0;i<artigoDB.size();i++)
        {

            if(artigoDB.get(i).artigo==artigo1.artigo){
                return artigoDB.get(i);
            }
        }

        //é suposto não chegar aqui, ou seja só procurar artigos que existam
        return null;
    }
    public int sizeOfArrayDB(){
        return artigoDB.size();
    }

    public int sizeOfArrayInscricoes(){
        return inscricoesDB.size();
    }

    public Artigo getNextArtigo(int atual){

        return  artigoDB.get(atual);
    }

    public Participante getNextParticipante(int atual){
        return inscricoesDB.get(atual);
    }

    public Revisor getRevisor(int atual)
    {
        return revisorDB.get(atual);
    }



    public Boolean containsArtigo(Artigo a1){
        return artigoDB.contains(a1);
    }

    public Boolean containsParticipante(Participante p1)
    {
        return inscricoesDB.contains(p1);
    }

    public Boolean containsRevisor(Revisor r1)
    {
        return revisorDB.contains(r1);
    }

}
