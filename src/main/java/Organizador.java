package main.java;

import java.util.Scanner;


public class Organizador {

    String nome;
    String email;
    int notaMinimaAprovacao;

    public Organizador(String nome, String email)
    {
        this.nome=nome;
        this.email=email;
        this.notaMinimaAprovacao=  2;// valor default antes de estabelecido o valor verdadeiro
    }

    public void setNotaMinimaAprovacao()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qual o valor mínimo para aprovação de um artigo: ");
        notaMinimaAprovacao=scanner.nextInt();
    }

    public int getNotaMinimaAprovacao()
    {
        return notaMinimaAprovacao;
    }

    //to fix
    public void avaliacaoArtigos(DB database){
        int contadorArtigos=0;
        int contadorRevisores=0;
        for(int i=0;i<database.sizeOfArrayDB();i++) {
            Artigo temp=database.getNextArtigo(contadorArtigos);
            int soma=0;
            while(temp.numeroDeRevisoes<3)
            {
                Revisor temp2;
                temp2=database.getRevisor(contadorRevisores);

                if(temp2!=temp.autor){
                    soma+=temp2.avaliacaoArtigo(temp);
                    temp.numeroDeRevisoes++;
                    temp.notaAtual=soma/temp.numeroDeRevisoes;

                }

            }

            if(temp.notaAtual>=notaMinimaAprovacao)
            {
                temp.aprovacao=true;
                System.out.println("Artigo aprovado");
            }
            else{
                System.out.println("Artigo reprovado");
            }
            contadorArtigos++;
            contadorRevisores++;
        }
    }
    public void printArtigos(DB database)
    {
        int contador=0;
        for(int i=0;i<database.sizeOfArrayDB();i++) {
            Artigo temp=database.getNextArtigo(contador);
            System.out.println(temp.artigo);
            contador++;
        }
    }

    public void printInscricoes(DB database)
    {
        int contador=0;
        for(int i=0;i<database.sizeOfArrayInscricoes();i++) {
            Participante temp=database.getNextParticipante(contador);
            System.out.println(temp.nome);
            contador++;
        }
    }
}
