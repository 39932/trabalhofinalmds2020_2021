package main.java;
public class Autor {


    String nome;
    String email;
    String instituicao;

    public Autor(String nome,String email, String instituicao){
        this.nome= nome;
        this.email=email;
        this.instituicao=instituicao;
    }

    public void setName(String nome){
        this.nome=nome;
    }

    public void setEmail( String email){
        this.email=email;
    }

    public void setInstituicao( String instituicao){
        this.instituicao=instituicao;
    }

    public String getName(){
        return this.nome;
    }

    public String getEmail(){
        return this.email;
    }

    public String getInstituicao(){
        return this.instituicao;
    }

    public boolean verificarEstado(DB artigoDB , Artigo artigo){
        Artigo temp;
        temp=artigoDB.getArtigoDB(artigo);

        return temp.aprovacao;
    }

    public void addArtigoToBD(DB artigoBD, Artigo artigo){
        artigoBD.addArtigoToDB(artigo);
    }
}
