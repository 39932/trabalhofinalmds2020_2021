package main.java;
import java.util.*;



public class Revisor extends Autor{

    public Revisor(String nome, String email, String instituicao) {
        super(nome, email, instituicao);
    }

    public int avaliacaoArtigo( Artigo artigo)
    {
        int temp;
        Scanner scanner = new Scanner(System.in);

        System.out.println(artigo.getArtigo());

        System.out.println("Qual a sua avaliação de 0 a 5:");
        temp=scanner.nextInt();

        return temp;
    }
    public void addRevisorToBD(DB database,Revisor revisor)
    {
        database.addRevisorToDB(revisor);
    }
}
