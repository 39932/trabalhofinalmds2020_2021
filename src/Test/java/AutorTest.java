package Test.java;

import main.java.Artigo;
import main.java.Autor;
import main.java.DB;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AutorTest {

    @Test
    public void verificaSetGetName(){
        Autor a1 = new Autor("Saramago","saramaguuh@hotmail.com","UE");
        a1.setName("Joaquim");

        assertEquals("Joaquim",a1.getName());
    }

    @Test
    public void verificaSetGetEmal(){
        Autor a1 = new Autor("ze","zeslb@gmail.com","circo");
        a1.setEmail("Ola");

        assertEquals("Ola",a1.getEmail());
    }
    @Test
    public void verificaSetGetInstituicao(){
        Autor a1= new Autor("Baby","yahoo@yahoo.com","fabrica");
        a1.setInstituicao("ue");

        assertEquals("ue",a1.getInstituicao());
    }
    @Test
    public void verificaVerificarEstado(){
        Autor b2= new Autor("germano","germano@bruh.com","SuP");

        DB database= new DB();
        Artigo a1= new Artigo( "asdad","asdad@gmail.com",null);
        database.addArtigoToDB(a1);

        assertFalse(b2.verificarEstado(database,a1));
    }

    @Test
    public void verificaAddArtigoToBD(){
        DB database = new DB();
        Autor a1 = new Autor("Francisco","dasmanias@gmail.com","ue");
        Artigo a2= new Artigo( "Pessequeiro Na ilha","Ola Amigos",null);
        a1.addArtigoToBD(database,a2);

        assertTrue(database.containsArtigo(a2));
    }
}
