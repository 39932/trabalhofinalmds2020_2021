package Test.java;

import main.java.Artigo;
import main.java.DB;
import main.java.Participante;
import main.java.Revisor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DBTest {

    @Test
    public void verificaAddInscriçãoToDB()
    {
        DB database = new DB();
        Participante p1= new Participante("asdad","asscvb");

        database.addInscricaoToDB(p1);

        assertTrue(database.containsParticipante(p1));
    }

    @Test
    public void VerificaAddArtigoToDB()
    {
        DB database = new DB();
        Artigo a1 = new Artigo("asdsad","Hue hue",null);

        database.addArtigoToDB(a1);

        assertTrue(database.containsArtigo(a1));
    }

    @Test
    public void VerificaAddRevisorToDB(){
        DB database = new DB();

        Revisor r1= new Revisor("Joaquim","ba@asd.com","Ui");
        database.addRevisorToDB(r1);

        assertTrue(database.containsRevisor(r1));
    }

    @Test
    public void VerificaSetArtigo(){
        DB database = new DB();

        Artigo a1= new Artigo("manel","hue",null);
        int contador=0;
        database.addArtigoToDB(a1);

        database.setArtigo(a1,contador);
        Artigo temp=database.getNextArtigo(contador);

        assertEquals(a1,temp);

    }

    @Test
    public void verificaGetArtigoDB(){
        DB database= new DB();

        Artigo a1 = new Artigo("oassd","Era uma vez",null);

        database.addArtigoToDB(a1);

        assertEquals(a1,database.getArtigoDB(a1));
    }

    @Test
    public void verificaSizeOfArrayDB()
    {
      DB database= new DB();
      Artigo a1= new Artigo("Ola","Shrek",null);
      database.addArtigoToDB(a1);

      assertEquals(1,database.sizeOfArrayDB());


    }

    @Test
    public void verificaSizeOfArrayInscricoes()
    {
        DB database= new DB();
        Participante p1= new Participante("Ola","Shrek");
        database.addInscricaoToDB(p1);

        assertEquals(1,database.sizeOfArrayInscricoes());

    }

    @Test
    public void verificaGetNextArtigo()
    {
        DB database= new DB();
        Artigo a1= new Artigo("Ola","Shrek",null);
        database.addArtigoToDB(a1);

        assertEquals(a1,database.getNextArtigo(0));
    }

    @Test
    public void verificaGetNextParticipante()
    {
        DB database= new DB();
        Participante p1 = new Participante("Ola","Valete");
        database.addInscricaoToDB(p1);

        assertEquals(p1,database.getNextParticipante(0));
    }

    @Test
    public void verificaGetRevisor(){
        DB database= new DB();
        Revisor r1 = new Revisor("ola","jamal@sada.pt","hue");
        database.addRevisorToDB(r1);

        assertEquals(r1,database.getRevisor(0));
    }

    @Test
    public void verificaContainsArtigo()
    {
        DB database= new DB();
        Artigo a1 = new Artigo("Joao","Miguel",null);
        database.addArtigoToDB(a1);

        assertTrue(database.containsArtigo(a1));
    }

    @Test
    public void containsParticipante()
    {
        DB database = new DB();
        Participante p1= new Participante("asdsad","asdad");
        database.addInscricaoToDB(p1);

        assertTrue(database.containsParticipante(p1));

    }

    @Test
    public void containsRevisor(){
        DB database = new DB();

        Revisor r1 = new Revisor("asdasd","asdsad","dgsds");
        database.addRevisorToDB(r1);

        assertTrue(database.containsRevisor(r1));


    }

}
