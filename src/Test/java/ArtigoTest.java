package Test.java;

import main.java.Artigo;
import main.java.Autor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArtigoTest {

    @Test
    public void verificaSetGetAutores()
    {
        Artigo a1 = new Artigo("Uma Bela Manha","Pradaria",null);

        Autor jose = new Autor("Jose da Silva","jsilva@gmail.com","UE");

        a1.setAutores(jose);

        assertEquals(a1.getAutores(),jose);
    }

    @Test
    public void verificaGetSetNumeroDeRevisoes()
    {
        short n1=2;
        Artigo a1= new Artigo("Heavy Metal nos anos 90...","Metallica",null);
        a1.setNumeroDeRevisoes(n1);

        assertEquals(n1,a1.getNumeroDeRevisoes());
    }
}
