package Test.java;

import main.java.Artigo;
import main.java.DB;
import main.java.Participante;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ParticipanteTest {

    @Test
    public void verificaSetGetName()
    {
        Participante p1= new Participante("ola john","martian@gmail.com");

        p1.setName("Marco");

        assertEquals("Marco",p1.getName());
    }

    @Test
    public void verificaSetGetEmail()
    {
        Participante p1= new Participante("ola john","martian@gmail.com");

        p1.setEmail("Joaquim@iol.pt");

        assertEquals("Joaquim@iol.pt",p1.getEmail());
    }

    @Test
    public void verificaAddParticipanteToBD()
    {
        DB database= new DB();
        Participante p1= new Participante("Eduardo","eduardo@gmail.com");


        p1.addParticipanteToBD(database,p1);

        assertTrue(database.containsParticipante(p1));
    }
}
