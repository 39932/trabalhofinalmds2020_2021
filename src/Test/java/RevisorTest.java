package Test.java;


import main.java.Artigo;
import main.java.DB;
import main.java.Revisor;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class RevisorTest{

    @Test
    public void verificaAvalicacaoArtigo(){
        Revisor rev = new Revisor("R","email123","ue" );
        Artigo a1=new Artigo("asda","asda",null);
        int avaliacao=3;

        String input = "3";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertEquals(avaliacao,rev.avaliacaoArtigo(a1));
    }

    @Test
    public void verificaAdicaoBD(){
        DB database = new DB();
        Revisor r1 = new Revisor("Francisco","Chicao@pt.pt","UE");
        r1.addRevisorToBD(database,r1);

        assertTrue(database.containsRevisor(r1));
    }
}
