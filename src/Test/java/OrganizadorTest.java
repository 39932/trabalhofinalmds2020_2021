package Test.java;

import main.java.*;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class OrganizadorTest {

    @Test
    public void verificaSetGetNotaMinimaAprovacao()
    {
        int nota= 3;

        Organizador orgi= new Organizador("Big Boss","boss@gmail.com");

        String input="3";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        orgi.setNotaMinimaAprovacao();

        assertEquals(nota,orgi.getNotaMinimaAprovacao());
    }

    @Test
    public void verificaAvaliaçãoArtigos()
    {
        Artigo a1= new Artigo("zeca foi as compras e comrpou leite","zeca",null);
        Revisor r1= new Revisor( "Joao","j@hotmail.com","ue");
        Revisor r2=new Revisor ("Ricardo","r@hotmail.com","rica");
        Revisor r3 = new Revisor ("Miguel","ma@gmail.com","rica");

        DB database = new DB();
        int soma=0;

        String input="3";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        soma+=r1.avaliacaoArtigo(a1);

        String input2="5";
        InputStream in2 = new ByteArrayInputStream(input2.getBytes());
        System.setIn(in2);
        soma+=r2.avaliacaoArtigo(a1);

        String input3="2";
        InputStream in3 = new ByteArrayInputStream(input3.getBytes());
        System.setIn(in3);
        soma+=r3.avaliacaoArtigo(a1);

        soma/=3;


        assertEquals(soma,3);

    }

    @Test
    public void verificaPrintArtigos()
    {
        DB database = new DB();
        Artigo a1= new Artigo("Uma historia sobre elefegantes no lago..","oalal",null);
        Organizador o1 = new Organizador("asda","ola@ola.pt");
        database.addArtigoToDB(a1);


        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));


       o1.printArtigos(database);

       String expected=a1.getArtigo().replaceAll("\n", "").replaceAll("\r", "");
       String actual=outContent.toString().replaceAll("\n", "").replaceAll("\r", "");
        assertEquals(expected,actual);

    }
}
