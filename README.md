# Relatório do Trabalho final de MDS
Francisco Pedro Nº39932


# Decisões no Trabalho

  Classes
- DB. A classe DB é a classe que vai gerir tudo o que se passa na conferência. Esta classe é responsável pelos Arrays Lists dos artigos, gestores e participantes da conferência. A informação a ser guardada de forma persistente foi escolhida por facilidade/necessidade. Estão também nesta class vários métodos de set/get e contains bastante usados pelas outras classes bem como os testes. Foi escolhido o uso de ArrayList pois é a estrutura com que eu estou mais confortável.
- Organizador. Na classe organizador existe um método para definição da nota miníma de artigo que neste caso vai ser recebido através de input de utilizador, um método para todo o processo de atribuição de artigos aos revisores e de avaliação dos artigos e ainda ambos os métodos para listar os artigos e inscrições.
    
- Participante. Na classe participante o único método excecional a get/set é usado para adicionar o participante da conferência há array list. Foi decidido que esta class teria um método para adicionar objects desta class há Array List visto que não há registo/autenticação o que faria com que este método seria inútil.

- Autor. Na classe Autor para além de get e set temos um método para verificar o estado do Artigo e ainda o método para adicionar o Artigo em si. 

- Artigo. Foi criado uma classe específica para o Artigo, que para além de ter uma String com o artigo em si como estipulado no enunciado, e do email/autor , foi adicionado um short numero de revisoes e um boolean de estado do trabalho para controlo e melhor masuneamento do artigo em si.
- Revisor. A classe Revisor dá extend da class Autor pois o revisor também pode submeter artigos. Para além dos métodos que partilha com a sua class super tem ainda o método de adicionar o revisor a lista semelhante ao que adicona organizadores e participantes e o método de avaliação que é usado pela classe Organizador para obter a avaliação final de um artigo.
- Main. A classe main apenas foi criada para exemplo de uma conferência funcional.




# Dificuldades

Houve por minha parte alguma dificuldade na criação dos testes pois é algo que não tenho muito hábito a fazer e é algo a melhorar no futuro. Devido à má gestão do tempo também houve alguns problemas em implementar tudo o que era pretendido pelo que o trabalho não está a 100%.












